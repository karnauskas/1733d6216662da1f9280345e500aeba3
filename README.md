# Slate config files

[Slate](https://github.com/jigish/slate) is a windows management application.  
Files must be placed in the user root folder `/Users/username`.

## GIT

These files can be found in this [Git repo](https://gist.github.com/alienlebarge/6808785).

## Todo

- [x] Send windows on the other screen
- [x] Remove the `Hyper section`
- [ ] Test [`.slate.js` JavaScript config file](https://github.com/jigish/slate/wiki/JavaScript-Configs)