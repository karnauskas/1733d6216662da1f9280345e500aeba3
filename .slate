# url: https://gist.github.com/alienlebarge/6808785

# Configs
config defaultToCurrentScreen true
config nudgePercentOf screenSize
config resizePercentOf screenSize
config secondsBetweenRepeat 0.1
config checkDefaultsOnLoad true
config focusCheckWidthMax 3000
config windowHintsShowIcons true
config windowHintsIgnoreHiddenWindows false
#config windowHintsDuration 5
config windowHintsSpread true
#config windowHintsOrder persist

# Monitor Aliases
alias screen-main      0
alias screen-second    1

# Misc Aliases
alias showHintsLeftHand hint AOEUIYQJKX
alias showNormalHint hint AOEUIDHTNSYXFBPKGMCW
alias browser 'Chrome'
alias editor 'SublimeText'

# Abstract positions
alias full          move screenOriginX;screenOriginY                  screenSizeX;screenSizeY
alias lefthalf      move screenOriginX;screenOriginY                  screenSizeX/2;screenSizeY
alias righthalf     move screenOriginX+screenSizeX/2;screenOriginY    screenSizeX/2;screenSizeY
alias rightmost     move screenOriginX+screenSizeX*0.4;screenOriginY  screenSizeX*0.6;screenSizeY
alias rightless     move screenOriginX+screenSizeX*0.6;screenOriginY  screenSizeX*0.4;screenSizeY
alias leftmost      move screenOriginX;screenOriginY                  screenSizeX*0.6;screenSizeY
alias leftless      move screenOriginX;screenOriginY                  screenSizeX*0.4;screenSizeY
# Concrete positions
alias 1-full        move screenOriginX;screenOriginY screenSizeX;screenSizeY         ${screen-main}
alias 1-left        move screenOriginX;screenOriginY screenSizeX/2;screenSizeY         ${screen-main}
alias 1-right       move screenOriginX+screenSizeX/2;screenOriginY screenSizeX/2;screenSizeY         ${screen-main}
alias 2-left        move screenOriginX;screenOriginY screenSizeX/2;screenSizeY         ${screen-second}
alias 2-right       move screenOriginX+screenSizeX/2;screenOriginY screenSizeX/2;screenSizeY         ${screen-second}
alias 2-righttop    move screenOriginX+screenSizeX/2;screenOriginY screenSizeX/2;screenSizeY/2       ${screen-second}
alias 2-rightbottom move screenOriginX+screenSizeX/2;screenOriginY+screenSizeY/2    screenSizeX/2;screenSizeY/2       ${screen-second}

# 2 Monitor layout
layout 2monitors 'iTerm':MAIN_FIRST                       ${2-rightbottom} | ${1-left}
layout 2monitors 'Safari':REPEAT                          ${1-full}
layout 2monitors 'Nightly':REPEAT                          ${1-full}
layout 2monitors 'Sublime Text 2':REPEAT              ${1-full}
layout 2monitors 'Mail':MAIN_FIRST                        ${2-left}
layout 2monitors 'MacVim':REPEAT                      ${1-full}
layout 2monitors 'Path Finder':MAIN_FIRST                 ${2-righttop}
layout 2monitors 'Xcode':REPEAT                           ${1-full}
layout 2monitors 'Eclipse':REPEAT                         ${1-full}
layout 2monitors 'iTunes':REPEAT                          ${1-full}

# 1 Monitor layout
layout 1monitor 'iTerm':MAIN_FIRST                       ${1-right}
layout 1monitor 'Sublime Text 2':MAIN_FIRST              ${1-left}
layout 1monitor 'MacVim':MAIN_FIRST                      ${1-left}
layout 1monitor 'Safari':REPEAT                          ${1-full}
layout 1monitor 'Nightly':REPEAT                          ${1-full}
layout 1monitor 'Mail':MAIN_FIRST                        ${1-full}
layout 1monitor 'Path Finder':MAIN_FIRST                 ${1-full}
layout 1monitor 'Xcode':REPEAT                           ${1-full}
layout 1monitor 'Eclipse':REPEAT                         ${1-full}
layout 1monitor 'iTunes':REPEAT                          ${1-full}

# Default Layouts
default 2monitors resolutions:${screen-main};${screen-second};
default 1monitor  resolutions:${screen-main}

# Numpad location Bindings
bind 0:ctrl ${showHintsLeftHand}
bind 1:ctrl push top bar-resize:screenSizeY/2
bind 2:ctrl ${leftless}
bind 3:ctrl ${lefthalf}
bind 4:ctrl ${leftmost}
bind 5:ctrl ${full}
bind 6:ctrl ${rightmost}
bind 7:ctrl ${righthalf}
bind 8:ctrl ${rightless}
bind 9:ctrl push bottom bar-resize:screenSizeY/2
bind g:ctrl grid padding:5 ${screen-main}:6,2 ${screen-second}:8,3
#bind pad+ throw next
#bind pad. focus behind
#bind pad* layout 2monitors
#bind pad/ layout 1monitor
bind e:ctrl  ${showNormalHint}
bind 1:ctrl;alt;cmd throw ${screen-main} resize
bind 2:ctrl;alt;cmd throw ${screen-second} resize